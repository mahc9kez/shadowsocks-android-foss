* 移除內建 QR Code 掃碼器
* 支援 Android 6.0+
* 預設路由為「略過區域網路」
* 使用 Cloudflare DNS 作為預設遠端 DNS
* 預設啟用 DNS 轉送
* 預設啟用 IPv6 路由
